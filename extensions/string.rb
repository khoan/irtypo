# String / File.join hack to cut some characters.
# https://github.com/juliocesar/hopla.io/blob/44f3afe1ccb6b90e5fe8ad9135ac18b47113ee28/Rakefile#L38
class String
  def /(to) File.join(self, to); end
end
