raise "Settings already has root" if Cuba.settings.has_key? :root

require 'pathname'

root = Pathname.new(__FILE__) + '../..'

Cuba.settings[:root] = root
