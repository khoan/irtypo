Touchtypist::Application.routes.draw do
  get '/qwerty' => 'qwerty#index', as: :qwerty
  get '/qwerty/:id' => 'qwerty#show', as: :qwerty_lesson

  root 'application#home'
end
