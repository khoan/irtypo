class Cuba
  raise "#layout already defined" if method_defined? :layout

  def layout name=:app
    if defined? @layouts
      return @layouts[name] if @layouts.has_key? name
    else
      @layouts = {}
    end

    require "./views/layouts/#{name}"

    @layouts[name] = Views::Layouts.const_get(name.capitalize).new
  end
end
