require 'sprockets'
require './extensions/string'

root = (Pathname.new(__FILE__)+'../..').to_s
assets = 'assets'
vendor = 'vendor'

AssetsServer = Sprockets::Environment.new root do |env|
  # Add all compass paths to it
  Compass.sass_engine_options[:load_paths].each do |path|
    env.prepend_path path.to_s
  end

  env.prepend_path assets/'javascripts'
  env.prepend_path assets/'stylesheets'
  env.prepend_path assets/'fonts'
  env.prepend_path vendor/assets/'javascripts'

  # Needed since Sprockets 2.5
  env.context_class.class_eval do
    def asset_path path, options={}; path end
  end
end
