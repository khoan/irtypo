require './config/app'
require './extensions/string'

root = Cuba.settings[:root].to_s

Mustache.template_path = root/'views'/'templates'
Mustache.view_namespace = 'Views'
