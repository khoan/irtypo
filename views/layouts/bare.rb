module Views
  module Layouts
    class Bare < Mustache
      attr_accessor :content

      # TODO replace with asset pipeline generated path
      def stylesheet_path
        '/assets/bare.css'
      end

      # TODO replace with asset pipeline generated path
      def javascript_path
        '/assets/bare.js'
      end
    end
  end
end
