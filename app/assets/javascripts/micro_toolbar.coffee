window.carousel = carousel = document.getElementById("table-of-content") or {}
carousel.forward = (n=1)-> carousel.scrollLeft += n
carousel.rewind = (n=1)-> carousel.scrollLeft -= n

forward = undefined
rewind = undefined

document.addEventListener "mouseover", (event) ->
  target = event.target

  if target.nodeName is "A"

    if target.href.match("#forward")
      forward = setInterval(carousel.forward, 20)

    else if target.href.match("#rewind")
      rewind = setInterval(carousel.rewind, 20)

document.addEventListener "mouseout", (event) ->
  if forward
    clearInterval forward
    forward = null
  else if rewind
    clearInterval rewind
    rewind = null

