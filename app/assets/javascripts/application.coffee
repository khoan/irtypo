#= require key_map
#= require donate
#= require micro_toolbar

document.addEventListener "keydown", (event) ->
  typed = event.keyCode
  next = typed is keys["↵"] and (if cursor then cursor.lastChild.data is "↵" else true) and @querySelector("link[rel=next]")
  if next
    event.preventDefault()
    window.location = next.href

exercise = document.querySelector("div[rel='exercise']")

return unless exercise

letters = document.createDocumentFragment()

i = 0
text = exercise[if ("innerText" of exercise) then "innerText" else "textContent"].trim()

while i < text.length
  key = document.createElement("span")
  letter = text[i]
  if letter is " "
    key.className += " space"
  else key.className += " enter"  if letter is "↵"
  key.appendChild document.createTextNode(letter)
  letters.appendChild key
  ++i

exercise.removeChild exercise.lastChild
exercise.appendChild letters

current = 0
cursor = exercise.children[current]
cursor.className += " on"
lookup_guide = (->
  guides = document.querySelector("ul[id='guide']")
  (index) ->
    guide = guides.querySelector("li[data-index='" + index + "']")
    unless guide
      guide = guides.children[index]
      guide = null  if guide and guide.hasAttribute("data-index") and index isnt guide.getAttribute("data-index")
    guide
)()

guide = lookup_guide(current)
guide.className += " on"  if guide

document.addEventListener "keypress", (event) ->
  return  if cursor.lastChild.data is "↵"

  event.preventDefault()

  wanted = keys[cursor.lastChild.data]
  typed = event.which

  cursor.className = cursor.className.replace("on", if typed is wanted then "match" else "conflict")

  guide.className = guide.className.replace(" on", "")  if guide
  ++current
  if current < exercise.children.length
    cursor = exercise.children[current]
    cursor.className += " on"
    guide = lookup_guide(current)
    guide = lookup_guide("last")  if not guide and current is (exercise.children.length - 1)
    guide.className += " on"  if guide
