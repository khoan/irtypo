document.addEventListener "click", (event) ->
  node = event.target
  node = node.parentNode if node.nodeName is "BDO"

  isDonationLink = node.nodeName is "A" and node.hash is "#donation"
  if isDonationLink
    event.preventDefault()
    donation = document.querySelector node.hash
    donation.submit()
